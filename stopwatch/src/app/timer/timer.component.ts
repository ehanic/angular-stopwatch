import {Component, OnDestroy, OnInit} from '@angular/core';
import { Subscription, interval } from 'rxjs';

@Component({
  selector: 'app-timer',
  templateUrl: './timer.component.html',
  styleUrls: ['./timer.component.css']
})
export class TimerComponent implements OnInit, OnDestroy {

  public started: boolean;
  public time: number;
  public newLap: number;
  public minutesDisplay: string;
  public hoursDisplay: string;
  public secondsDisplay: string;
  public millisecondsDisplay: string;
  public minutesDisplayLap: string;
  public hoursDisplayLap: string;
  public secondsDisplayLap: string;
  public millisecondsDisplayLap: string;
  public laps: string[][];
  public sub: Subscription;
  public CONSTANTS = {
    HOUR: 360000,
    MINUTE: 6000,
    SECOND: 60,
    DIVIDER: 100,
    PERIOD: 10,
    NUM_LIMITER: 9,
  };

  constructor() {
    this.reset();
  }

  private startTimer() {
    const intervalLocal = interval(10);
    this.sub = intervalLocal.subscribe(
      () => {
        /**
         * Condition which indicate to refresh time or not
         */
        if (!this.started) {
          return;
        }
        /**
         * Tick counters
         */
        this.newLap++;
        this.time++;
        /**
         * Get total time
         */
        this.hoursDisplay = this.getHourOrMinute(this.time, this.CONSTANTS.HOUR);
        this.minutesDisplay = this.getHourOrMinute(this.time, this.CONSTANTS.MINUTE);
        this.secondsDisplay = this.getSecond(this.time, this.CONSTANTS.SECOND);
        this.millisecondsDisplay = this.getMillisecond(this.time);
        /**
         * Get time for lap
         */
        this.hoursDisplayLap = this.getHourOrMinute(this.newLap, this.CONSTANTS.HOUR);
        this.minutesDisplayLap = this.getHourOrMinute(this.newLap, this.CONSTANTS.MINUTE);
        this.secondsDisplayLap = this.getSecond(this.newLap, this.CONSTANTS.SECOND);
        this.millisecondsDisplayLap = this.getMillisecond(this.newLap);
      });
  }

  private getHourOrMinute(time: number, constant: number): string {
    return Math.floor(time / constant) <= this.CONSTANTS.NUM_LIMITER ?
      '0' + Math.floor(time / constant) :
      Math.floor(time / constant).toString();
  }

  private getSecond(time: number, constant: number): string {
    return Math.floor(time / this.CONSTANTS.DIVIDER % constant) <= this.CONSTANTS.NUM_LIMITER ?
      '0' + Math.floor(time / this.CONSTANTS.DIVIDER % constant) :
      Math.floor(time / this.CONSTANTS.DIVIDER % constant).toString();
  }

  private getMillisecond(time: number): string {
    return time % this.CONSTANTS.DIVIDER <= this.CONSTANTS.NUM_LIMITER ?
      '0' + time % this.CONSTANTS.DIVIDER :
      time % this.CONSTANTS.DIVIDER + '';
  }

  private start() {
    this.started = true;
  }

  private stop() {
    this.started = false;
  }

  private lap() {
    if (this.started) {
      this.laps.push([
        `${this.hoursDisplay} : ${this.minutesDisplay} : ${this.secondsDisplay} : ${this.millisecondsDisplay}`,
        `${this.hoursDisplayLap} : ${this.minutesDisplayLap} : ${this.secondsDisplayLap} : ${this.millisecondsDisplayLap}`
      ]);
      this.newLap = 0;
    }
  }

  private reset() {
    this.started = false;
    this.time = 0;
    this.newLap = 0;
    this.minutesDisplay = '00';
    this.hoursDisplay = '00';
    this.secondsDisplay = '00';
    this.millisecondsDisplay = '00';
    this.minutesDisplayLap = '00';
    this.hoursDisplayLap = '00';
    this.secondsDisplayLap = '00';
    this.millisecondsDisplayLap = '00';
    this.laps = [];
  }

  ngOnInit() {
    this.startTimer();
  }

  ngOnDestroy() {
    this.sub.unsubscribe();
  }

}
